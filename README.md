# CS298 Proposal


# Interactive Computer Science Exercises in EdX

Hong Le (hongnhung1636@gmail.com)

**Advisor**: Dr. Cay Horstmann (cay.horstmann@sjsu.edu)

**Committee Members:**
## Description:

Massive Open Online Courses (MOOCs) has become very popular in modern technology. It offers online courses/lectures for people of all ages and different educational backgrounds. It helps people taking courses online without wasting money and time on transportations, commutes, and tuition fees. However, based on professor Gou from University of Rochester’s studies, students who paid for online courses only view on average 78% of the course's content and skipped 22% of it. It also means that viewers who do not pay for the courses would skip more than that. It has become a big concern for professors since their students do not express a good understanding of materials being taught because they do not watch the lecture seriously. That’s when interactive courses come in handy. Edx offers interactive online courses and it also provides open source platform, Open Edx, which powers their courses. Xblock is one of edx’s component architecture extensions.
    
Currently, there is no XBlock component within Edx that provides checking the code created by students to let them know whether their code are correct or not. Codecheck (http://horstmann.com/codecheck/index.html), which was developed under professor Cay Horstmann, is a unit-testing library for automatic evaluation of student's coding problem. It provides instant response for students if their code answers are correct or need to be fixed. For this project, I want to implement an XBlock component, which will encapsulate questions and student's feedback from Codecheck inside of XBlock.

I want to create an XBlock that is linked with Codecheck for students to work on their course's exercises. Students will be able to answer the question from Codecheck and received the feedback from it within XBlock environment. It will also allowed students to view their work, edit their work, and save their work for later. Staff will be able control all the functionalities of the XBlock with a basic interface.


##CS297 Results:

For the duration of CS297, I have completed setting up the environment and partially implemented the script for Codecheck XBlock.

I installed DevStack on my local machine for development purposes and FullStack on an AWS instance for display purposes. DevStack (Developer Stack) and FullStack are two Vagrant configuration of Open Edx . DevStack is used for development phase and FullStack is used for production phase. I was able to implement to display Codecheck content on Edx. After installed the implemented XBlock on OpenEdx, when users enter the Codecheck's URL and name, the problem's content and name will be displayed on the course.


## Proposed Schedule:##
Timeline  | Task
------------- | -------------
Week 1 |Work on the proposal. Meet with Committee members. 
Week 2,3 |Work on Xblock handler 
Week 4,5 |Deliverable#1:Implementing the handler between Codecheck and XBlock on student’s feedback 
Week 6 |Study how to save student's answer for editing 
Week 7,8 |Deliverable#2:Keeping track of student's feedback to allow student edit or submit
Week 9 |Study get student's grading on Edx 
Week 10 |Deliverable#3: Keeping track of student's grading and activities
Week 11 |Deliverable#4:Testing the program 
Week 12 |Create a first draft of CS298 report 
Week 13,14 |Create a final CS298 Report and submit to Advisor and committee members. 
Week 15 |Defense

##Key Deliverables:

### Software

* Implementing the handler between Codecheck and XBlock on student’s feedback

* Keeping track of student's feedback to allow student edit or submit 

* Keeping track of student's grading and activities

* Testing the program

###Report

* CS298 Report
* Code Documentation


## Innovations and Challenges

* OpenEdx is a new platform on MOOC and it has a promising future for online learning.

* The installation instructions on the edx website are very simple, however, there were many bugs during the installation phase.

*  Develop a component within Edx that provides checking the code created by students to let them know whether their code are correct or not.

## References:

Gou, P. & Reinecke, K. (2014) Demographic differences in how students navigate through MOOCs. Retrieved from http://dl.acm.org.libaccess.sjlibrary.org/citation.cfm?id=2556325.2566247&coll=DL&dl=ACM&CFID=706615951&CFTOKEN=54198283 

Shirai, S. (2014) The Crowd Sourced Hinter For edX Online Courses. Retrieved from http://web.mit.edu/rsi/2014/all/sola.pdf.gz

Installing, Configuring, and Running the Open edX Platform. Edx, 2015. [Online]. Available: http://edx.readthedocs.org/projects/edx-installing-configuring-and-running/en/latest/index.html.